import { Regiao } from './../../models/regiao';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RegiaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-regiao',
  templateUrl: 'regiao.html',
})
export class RegiaoPage {
  regiao: Regiao;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.regiao = this.navParams.get('regiao');
  }

}
