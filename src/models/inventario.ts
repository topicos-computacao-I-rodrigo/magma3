import { Item } from './item';

export class Inventario {
  elmo?: Item;
  arma?: Item;
  armadura?: Item;
  luvas?: Item;
  calcas?: Item;
  botas?: Item;
  mochila: Item[][];
}