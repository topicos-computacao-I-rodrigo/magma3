import { Monstro } from './monstro';


export class Regiao {
  nome: string;
  nivelInicial: number;
  nivelFinal: number;
  cor: string;
  monstros: Monstro[];
}