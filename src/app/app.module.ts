import { MagmaItemComponent } from './../components/magma-item/magma-item';
import { MagmaInventarioComponent } from './../components/magma-inventario/magma-inventario';
import { InventarioPage } from './../pages/inventario/inventario';
import { Inventario } from './../models/inventario';
import { MundoPage } from './../pages/mundo/mundo';
import { TabsPage } from './../pages/tabs/tabs';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { RegiaoPage } from '../pages/regiao/regiao';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    MundoPage,
    RegiaoPage,
    InventarioPage,
    MagmaInventarioComponent,
    MagmaItemComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    MundoPage,
    RegiaoPage,
    InventarioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
