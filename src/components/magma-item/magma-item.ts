import { Item } from './../../models/item';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'magma-item',
  templateUrl: 'magma-item.html'
})
export class MagmaItemComponent {
  @Input() item: Item;
  @Input() tipo: string;

  constructor() {
    
  }

}
