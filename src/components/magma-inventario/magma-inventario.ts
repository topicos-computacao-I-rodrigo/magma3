import { Inventario } from './../../models/inventario';
import { Item } from './../../models/item';
import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'magma-inventario',
  templateUrl: 'magma-inventario.html',
})
export class MagmaInventarioComponent {
  inventario: Inventario = {
    mochila: [
      [null, null, null, null],
      [null, null, null, null],
      [null, null, null, null]
    ]
  };

  constructor() {
    let itemArma: Item = {
      nome: 'Espada Curta',
      imagem: 'assets/itens/espada_curta.png'
    }

    let itemPocaoVida: Item = {
      nome: 'Poção de Vida',
      imagem: 'assets/itens/pocao_vida.png'
    }

    let itemElmo: Item = {
      nome: 'Elmo de Ferro',
      imagem: 'assets/itens/elmo.png'
    }

    this.inventario.elmo = itemElmo;
    this.inventario.mochila[0][0] = itemPocaoVida;
    this.inventario.mochila[0][1] = itemPocaoVida;
    this.inventario.mochila[0][2] = itemPocaoVida;
    this.inventario.mochila[0][3] = itemArma;
    this.inventario.mochila[1][0] = itemElmo;
  }
}
